# Additional references

## General introduction to FEniCSx

* [The FEniCSx tutorial](https://jsdokken.com/dolfinx-tutorial/index.html) by Jørgen S. Dokken
* [The `dolfinx` documentation](https://docs.fenicsproject.org/dolfinx/main/python/)

## Legacy FEniCS references

* [The FEniCS book](https://doi.org/10.1007/978-3-319-52462-7) by Hans Petter Langtangen and Anders Logg. Solving PDEs in Python: The FEniCS Tutorial I. Springer International Publishing, Cham, 2016. ISBN 978-3-319-52462-7. 
* [Numerical tours of Computational Mechanics using FEniCS](https://comet-fenics.readthedocs.io/) by Jeremy Bleyer: includes thermoelasticity, viscoelasticity, plasticity, buckling, beams, plates, shells, homogenization, cohesive zone models, etc.

## Add-ons to the FEniCSx ecosystem

* [`dolfinx-mpc`](https://github.com/jorgensd/dolfinx_mpc) by Jørgen S. Dokken: an extension to dolfinx to handle multi-point constraints (master/slave dofs, slip conditions, periodicity, etc.)
* [`fenicsx-shells`](https://github.com/fenics-shells/fenicsx-shells) by Jack Hale, Corrado Maurini: a library for simulating thin structures
* [`multiphenicsx`](https://github.com/multiphenics/multiphenicsx) by Francesco Ballarin: easy prototyping of multiphysics problems on conforming meshes.
* [`RBniCSx`](https://github.com/RBniCS/RBniCSx) by Francesco Ballarin: reduced order modelling in FEniCSx.

## Add-ons to the FEniCS ecosystem

* [`mgis.fenics`](https://thelfer.github.io/mgis/web/mgis_fenics.html) by Jeremy Bleyer and Thomas Helfer: support to nonlinear constitutive laws implemented with the [MFront](https://tfel.sourceforge.net/documentations.html) code generator
* [`fenics_optim`](https://fenics-optim.readthedocs.io/) by Jeremy Bleyer: a convex optimization interface in FEniCS, useful to solve non-smooth constrained convex optimization problems.
* [`dolfin-adjoint`](http://www.dolfin-adjoint.org) automatically derives the discrete adjoint and tangent linear models from a forward model, useful for data assimilation, optimal control, sensitivity analysis, design optimisation, and error estimation.