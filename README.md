# FEniCS-x tutorials
Author: Jérémy Bleyer (jeremy.bleyer@enpc.fr), Laboratoire Navier, (Ecole des Ponts, Univ Gustave Eiffel, CNRS). 

![Navier](https://navier-lab.fr/wp-content/uploads/2021/11/NAVIER-LOGO-COULEUR-RVB-ECRAN-72ppp_pour-site.png)

## Contents

![FEniCS](logo.png)

This repository contains tutorials to the FEniCSx software (https://fenicsproject.org/).

Examples cover solid and structural mechanics applications and showcase:
* how to define a linear problem, apply boundary conditions, solve for the solution and output to a results file
* how to define a nonlinear problem and use automatic differentiation for easier computations
* how to define a problem involving multiple mechanical fields using mixed function spaces
* how to set up an eigenvalue problem

[DEMO 1: Linear elasticity](demos/linear_elasticity/linear_elasticity.ipynb)

[DEMO 2: Hyperelasticity](demos/hyperelasticity/hyperelasticity.ipynb)

[DEMO 3: Reissner-Mindlin plates](demos/plates/plates.ipynb)

[To finish: additional references](references.md)

## Installation

First, you will need **Paraview** to visualize the results. You can [download the latest version here](https://www.paraview.org/download/).

For **FEniCSx**, general installation guidelines are available at https://github.com/FEniCS/dolfinx#installation. 
We recommend using `conda` for MacOS and Linux users. Note that for Windows users, the `conda` packages are not supported, see [the Windows section](#for-windows).

### For MacOS and Linux using `conda`

To install the latest stable release of the Python interface using conda:

```
conda create -n fenicsx-env
conda activate fenicsx-env
conda install -c conda-forge fenics-dolfinx mpich
```

conda is distributed with Anaconda and Miniconda. The conda recipe is hosted on conda-forge.

### For Windows

FEniCS is not distributed for Windows boxes. 

#### Windows Subsystem for Linux

For Windows 10, the preferred option is the Windows Subsystem for linux (WSL). Install the Ubuntu distribution as WSL, then refer to the section above inside the Ubuntu WSL.

#### Docker

Download [Docker Desktop for Windows](https://docs.docker.com/desktop/install/windows-install/).

Then, you can get a Docker image of DOLFINx v0.6.0 with:

```
docker run -ti -v %cd%:/root/shared -w /root/shared dolfinx/dolfinx:v0.6.0
```
on Windows and:
```
docker run -ti -v $(pwd):/root/shared -w /root/shared dolfinx/dolfinx:v0.6.0
```
otherwise.

A Jupyter Lab environment with the latest stable release of DOLFINx on Windows:

```
docker run -ti -v %cd%:/root/shared -w /root/shared --rm -p 8888:8888  dolfinx/lab:v0.6.0  # Access at http://localhost:8888
```

and on non-Windows:

```
docker run -ti -v $(pwd):/root/shared -w /root/shared --rm -p 8888:8888  dolfinx/lab:v0.6.0  # Access at http://localhost:8888
```
