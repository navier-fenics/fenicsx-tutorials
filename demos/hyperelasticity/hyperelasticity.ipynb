{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d2f54a50-ae21-4f3a-b756-8429b423a2ff",
   "metadata": {},
   "source": [
    "$$\\newcommand{\\bsig}{\\boldsymbol{\\sigma}}\n",
    "\\newcommand{\\beps}{\\boldsymbol{\\varepsilon}}\n",
    "\\newcommand{\\bu}{\\boldsymbol{u}}\n",
    "\\newcommand{\\bv}{\\boldsymbol{v}}\n",
    "\\newcommand{\\bT}{\\boldsymbol{T}}\n",
    "\\newcommand{\\bC}{\\boldsymbol{C}}\n",
    "\\newcommand{\\bF}{\\boldsymbol{F}}\n",
    "\\newcommand{\\bI}{\\boldsymbol{I}}\n",
    "\\newcommand{\\bP}{\\boldsymbol{P}}\n",
    "\\newcommand{\\dOm}{\\,\\text{d}\\Omega}\n",
    "\\newcommand{\\dS}{\\,\\text{d}S}\n",
    "\\newcommand{\\T}{{}^\\text{T}}\n",
    "\\newcommand{\\tr}{\\operatorname{tr}}\n",
    "\\newcommand{\\Neumann}{{\\partial \\Omega_\\text{N}}}\n",
    "\\newcommand{\\Dirichlet}{{\\partial \\Omega_\\text{D}}}\n",
    "\\newcommand{\\argmin}{\\operatorname*{arg\\,min}}\n",
    "$$\n",
    "# Hyperelasticity\n",
    "\n",
    "In this tour, we will build upon the previous one on linear elasticity to formulate a non-linear problem arising from finite-strain elasticity. The symbolic differentiation capabilities of UFL prove to be very useful in such examples.\n",
    "\n",
    "## Variational formulation\n",
    "\n",
    "There exist different ways of writing a variational formulation in a finite-strain setting depending on the chosen geometric configuration and strain measures. Here, we will use a total Lagrangian formulation and therefore naturally write equilibrium on the reference configuration which we still denote $\\Omega$.\n",
    "\n",
    "Weak equilibrium reads here:\n",
    "> Find $\\bu \\in V$ such that:\n",
    "> \\begin{equation}\n",
    "\\int_\\Omega \\bP(\\bu):\\nabla \\bv \\dOm = \\int_\\Omega \\boldsymbol{f}\\cdot\\bv \\dOm + \\int_\\Neumann \\bT\\cdot\\bv \\dS \\quad \\forall \\bv \\in V_0\n",
    "\\end{equation}\n",
    "\n",
    "where $\\bP(\\bu)$ denotes the first Piola-Kirchhoff (PK1) stress.\n",
    "\n",
    "Moreover, in the case of a hyperelastic material, the constitutive relation derives from a free-energy potential $\\psi(\\bF)$ depending on the deformation gradient $\\bF = \\bI + \\nabla \\bu$. The above non-linear variational equation corresponds in fact to the first-order optimality condition of the following minimum principle:\n",
    "> \\begin{equation}\n",
    "\\min_{\\bu\\in V} \\int_\\Omega \\psi(\\bF) \\dOm - \\int_\\Omega \\boldsymbol{f}\\cdot\\bu \\dOm - \\int_\\Neumann \\bT\\cdot\\bu \\dS\n",
    "\\end{equation}\n",
    "\n",
    "which we will use in the subsequent implementation.\n",
    "\n",
    "## Problem position\n",
    "\n",
    "We consider a cyclinder of square cross-section which is fixed at its bottom face and to which we impose a rigid rotation of the top face of angle $\\theta$ around the vertical axis. We start first with a simple compressible neo-Hookean model given by:\n",
    "\n",
    "\\begin{equation}\n",
    "\\psi(\\bF) = \\dfrac{\\mu}{2}\\left(I_1-3-2\\ln J\\right) + \\dfrac{\\lambda}{2}(J-1)^2\n",
    "\\end{equation}\n",
    "\n",
    "where $I_1 = \\tr(\\bC) = \\tr(\\bF\\T\\bF)$ and $J = \\det\\bF$.\n",
    "\n",
    "## Implementation\n",
    "\n",
    "We load the relevant modules and useful functions and setup the corresponding box mesh. In the following, we will use hexahedra of degree 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "8ebf1708-bfc3-4baa-9a2f-3a229a5cb8b9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Mesh topology dimension d=3.\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import ufl\n",
    "\n",
    "from mpi4py import MPI\n",
    "from dolfinx import fem, io, nls\n",
    "from dolfinx.mesh import create_box, CellType\n",
    "from ufl import (\n",
    "    as_matrix,\n",
    "    dot,\n",
    "    cos,\n",
    "    sin,\n",
    "    SpatialCoordinate,\n",
    "    Identity,\n",
    "    grad,\n",
    "    ln,\n",
    "    tr,\n",
    "    det,\n",
    "    variable,\n",
    "    derivative,\n",
    "    TestFunction,\n",
    "    TrialFunction\n",
    ")\n",
    "L = 3.0\n",
    "N = 8\n",
    "mesh = create_box(\n",
    "    MPI.COMM_WORLD, [[-0.5, -0.5, 0.0], [0.5, 0.5, L]], [N, N, 4*N], CellType.hexahedron\n",
    ")\n",
    "\n",
    "dim = mesh.topology.dim\n",
    "print(f\"Mesh topology dimension d={dim}.\")\n",
    "\n",
    "degree = 1\n",
    "V = fem.VectorFunctionSpace(mesh, (\"CG\", degree))\n",
    "\n",
    "u = fem.Function(V, name=\"Displacement\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a17074c4-449e-4672-aa97-0d63f6d68a17",
   "metadata": {},
   "source": [
    "Next, we define the corresponding hyperelastic potential using UFL operators. We can easily obtain the UFL expression for the PK1 stress by differentiating the potential $\\psi$ with respect to the deformation gradient $\\bF$. We therefore declare it as a variable using `ufl.variable` and then compute $\\bP = \\dfrac{\\partial \\psi}{\\partial \\bF}$ using `ufl.diff`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "91fc7b57-cf20-4a44-8269-106c8f9b2f1a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "d/d[var0(I + (grad(Displacement)))] ((-3 + (tr({ A | A_{i_8, i_9} = sum_{i_{10}} (var0(I + (grad(Displacement))))[i_{10}, i_9] * ((var0(I + (grad(Displacement))))^T)[i_8, i_{10}]  })) + -1 * 2 * ln(det(var0(I + (grad(Displacement)))))) * c_0 / 2 + c_1 / 2 * (-1 + (det(var0(I + (grad(Displacement)))))) ** 2)\n"
     ]
    }
   ],
   "source": [
    "# Identity tensor\n",
    "Id = Identity(dim)\n",
    "\n",
    "# Deformation gradient\n",
    "F = variable(Id + grad(u))\n",
    "\n",
    "# Right Cauchy-Green tensor\n",
    "C = F.T * F\n",
    "\n",
    "# Invariants of deformation tensors\n",
    "I1 = tr(C)\n",
    "J = det(F)\n",
    "\n",
    "# Shear modulus\n",
    "E = 1e4\n",
    "nu = 0.4\n",
    "mu = fem.Constant(mesh, E / 2 / (1 + nu))\n",
    "lmbda = fem.Constant(mesh, E * nu / (1 - 2 *nu) / (1 + nu))\n",
    "\n",
    "# Stored strain energy density (compressible neo-Hookean model)\n",
    "psi = mu / 2 * (I1 - 3 - 2 * ln(J)) + lmbda / 2 * (J - 1) ** 2\n",
    "\n",
    "# PK1 stress = d_psi/d_F\n",
    "P = ufl.diff(psi, F)\n",
    "print(P)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35010eb7-9e4e-47d4-ba5b-3deb84af555d",
   "metadata": {},
   "source": [
    "Now, we set up the boundary conditions by first identifying the top and bottom dofs. We use Functions to provide the imposed displacement on both faces. For now, such functions are zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "c3346c4d-d569-4e35-aaf0-012ae7e7d804",
   "metadata": {},
   "outputs": [],
   "source": [
    "def bottom(x):\n",
    "    return np.isclose(x[2], 0)\n",
    "\n",
    "def top(x):\n",
    "    return np.isclose(x[2], L)\n",
    "\n",
    "bottom_dofs = fem.locate_dofs_geometrical(V, bottom)\n",
    "top_dofs = fem.locate_dofs_geometrical(V, top)\n",
    "\n",
    "u_bot = fem.Function(V)\n",
    "u_top = fem.Function(V)\n",
    "\n",
    "bcs = [fem.dirichletbc(u_bot, bottom_dofs), fem.dirichletbc(u_top, top_dofs)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3743794-83a3-4e0d-b2f2-b281fab7c12f",
   "metadata": {},
   "source": [
    "We will later update the value of the `u_top` function based on a UFL expression corresponding to the imposed rigid body rotation. This expression depends on a scalar value $\\theta$ represented as a `Constant` object. The use of a `fem.Expression` results in JIT compilation of the code corresponding to the evaluation of this expression at specific points in the reference elements (here the interpolation points of $V$ i.e. the hexahedron vertices)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "89a8447f-51a0-407d-af7a-931f73e1d1f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = SpatialCoordinate(mesh)\n",
    "theta = fem.Constant(mesh, 0.0)\n",
    "Rot = as_matrix([[cos(theta), sin(theta), 0], \n",
    "                 [-sin(theta), cos(theta), 0], \n",
    "                 [0, 0, 1]])\n",
    "rotation_displ = dot(Rot, x) - x\n",
    "rot_expr = fem.Expression(rotation_displ, V.element.interpolation_points())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1354712-6aad-4898-9389-238ecaad5a2c",
   "metadata": {},
   "source": [
    "Now, we define the global non-linear potential energy. Note that since we have non-linear expressions, we specify to the measure `dx` the desired level of accuracy of the quadrature method. Otherwise, FEniCS may use overly conservative estimates of the required number of quadrature points.\n",
    "\n",
    "Next, we compute the corresponding non-linear residual using the `ufl.derivative` function which computes the directional derivative in the direction of the TestFunction `v`.\n",
    "We also apply it to the residual itself to compute the corresponding consistent tangent bilinear form, usually called the Jacobian in the context of a Newton method. The latter is computed in the direction of the TrialFunction `du`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "b4b41316-ecef-4ba1-b21f-bec9e49bae6d",
   "metadata": {},
   "outputs": [],
   "source": [
    "dx = ufl.Measure(\"dx\", domain=mesh, metadata={\"quadrature_degree\": 4})\n",
    "E_pot = psi*dx\n",
    "\n",
    "v = TestFunction(V)\n",
    "du = TrialFunction(V)\n",
    "Residual = derivative(E_pot, u, v) # This is equivalent to Residual = inner(P, grad(v))*dx\n",
    "Jacobian = derivative(Residual, u, du)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6894f615-303c-4b0b-8e05-ee5142939946",
   "metadata": {},
   "source": [
    "Finally, we set up a `NonlinearProblem` instance based on the corresponding residual and jacobian, unknown function and boundary conditions. The latter will also be attached to a nonlinear solver implementing a Newton method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "14ed3b50-5093-4045-9524-eda59495fb0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "problem = fem.petsc.NonlinearProblem(Residual, u, bcs)\n",
    "\n",
    "solver = nls.petsc.NewtonSolver(mesh.comm, problem)\n",
    "# Set Newton solver options\n",
    "solver.atol = 1e-4\n",
    "solver.rtol = 1e-4\n",
    "solver.convergence_criterion = \"incremental\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01eb4616-a993-4e88-b781-957c1d0ece89",
   "metadata": {},
   "source": [
    "We are now in position to write the load-stepping loop which simply updates the value of $\\theta$. Since, `rot_expr` is symbolically linked to `theta`, this new value is automatically accounted for when interpolating the imposed top displacement from `rot_expr`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f7d7e204-80a8-4cdc-9da3-018dd2efd854",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Time step 0, Number of iterations 5, Angle 24 deg.\n",
      "Time step 1, Number of iterations 5, Angle 48 deg.\n",
      "Time step 2, Number of iterations 5, Angle 72 deg.\n",
      "Time step 3, Number of iterations 5, Angle 96 deg.\n",
      "Time step 4, Number of iterations 5, Angle 120 deg.\n",
      "Time step 5, Number of iterations 5, Angle 144 deg.\n",
      "Time step 6, Number of iterations 6, Angle 168 deg.\n",
      "Time step 7, Number of iterations 6, Angle 192 deg.\n",
      "Time step 8, Number of iterations 6, Angle 216 deg.\n",
      "Time step 9, Number of iterations 6, Angle 240 deg.\n",
      "Time step 10, Number of iterations 6, Angle 264 deg.\n",
      "Time step 11, Number of iterations 6, Angle 288 deg.\n",
      "Time step 12, Number of iterations 6, Angle 312 deg.\n",
      "Time step 13, Number of iterations 6, Angle 336 deg.\n",
      "Time step 14, Number of iterations 6, Angle 360 deg.\n"
     ]
    }
   ],
   "source": [
    "angle_max = 2*np.pi\n",
    "Nsteps = 15\n",
    "\n",
    "out_file = \"hyperelasticity.xdmf\"\n",
    "with io.XDMFFile(mesh.comm, out_file, \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "\n",
    "u.vector.set(0.0)\n",
    "for n, angle in enumerate(np.linspace(0, angle_max, Nsteps+1)[1:]):\n",
    "    theta.value = angle\n",
    "    u_top.interpolate(rot_expr)\n",
    "    \n",
    "    num_its, converged = solver.solve(u)\n",
    "    assert converged\n",
    "    \n",
    "    u.x.scatter_forward() # updates ghost values for parallel computations\n",
    "    \n",
    "    print(\n",
    "        f\"Time step {n}, Number of iterations {num_its}, Angle {angle*180/np.pi:.0f} deg.\"\n",
    "    )\n",
    "    \n",
    "    with io.XDMFFile(mesh.comm, out_file, \"a\") as xdmf:\n",
    "        xdmf.write_function(u, n+1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a0e1b9c-72b5-4ad4-a9cf-f5c51ae44f17",
   "metadata": {},
   "source": [
    "### Exercise\n",
    "\n",
    "Change the hyperelasticity model to another version of a compressible neo-Hookean model:\n",
    "\\begin{equation}\n",
    "\\psi(\\bF) = \\dfrac{\\mu}{2}(\\overline{I}_1-3) + \\left(\\dfrac{\\mu}{12}+\\dfrac{\\lambda}{8}\\right)\\left(J^2 + J^{-2}-2 \\right)\n",
    "\\end{equation}\n",
    "where $\\overline{I}_1 = \\tr(\\overline{\\bC})$ with $\\overline{\\bC} = J^{-2/3}\\bC$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5770dd7-f95f-4001-9c21-76259cbc896b",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
