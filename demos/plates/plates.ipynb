{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f729164e-a901-41dd-8776-318971da3e45",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Reissner-Mindlin plates\n",
    "$$\\newcommand{\\bM}{\\boldsymbol{M}}\n",
    "\\newcommand{\\bQ}{\\boldsymbol{Q}}\n",
    "\\newcommand{\\bgamma}{\\boldsymbol{\\gamma}}\n",
    "\\newcommand{\\btheta}{\\boldsymbol{\\theta}}\n",
    "\\newcommand{\\bchi}{\\boldsymbol{\\chi}}\n",
    "\\renewcommand{\\div}{\\operatorname{div}}$$\n",
    "\n",
    "This demo illustrates how to implement a Reissner-Mindlin thick plate model. The main specificity of such models is that one needs to solve for two different fields: a vertical deflection field $w$ and a rotation vector field $\\btheta$. We recall below the main relations defining this model in the linear elastic case.\n",
    "\n",
    "## Governing equations\n",
    "### Generalized strains\n",
    "\n",
    "* *Bending curvature* strain $\\bchi = \\dfrac{1}{2}(\\nabla \\btheta + \\nabla^\\text{T} \\btheta) = \\nabla^\\text{s}\\btheta$\n",
    "* *Shear* strain $\\bgamma = \\nabla w - \\btheta$\n",
    "\n",
    "### Generalized stresses \n",
    "* *Bending moment* $\\bM$\n",
    "* *Shear force* $\\bQ$\n",
    "\n",
    "### Equilibrium conditions\n",
    "For a distributed transverse surface loading $f$,\n",
    "* Vertical equilibrium: $\\div \\bQ + f = 0$\n",
    "* Moment equilibrium: $\\div \\bM + \\bQ = 0$\n",
    "\n",
    "In weak form:\n",
    "$$\\int_{\\Omega} (\\bM:\\nabla^\\text{s}\\widehat{\\btheta} + \\bQ\\cdot(\\nabla \\widehat{w} - \\widehat{\\btheta}))\\text{d}\\Omega = \\int_{\\Omega} f w \\text{d}\\Omega \\quad \\forall \\widehat{w},\\widehat{\\btheta}$$\n",
    "\n",
    "### Isotropic linear elastic constitutive relation\n",
    "* Bending/curvature relation:\n",
    "\\begin{align*}\n",
    "\\begin{Bmatrix}M_{xx}\\\\ M_{yy} \\\\M_{xy} \\end{Bmatrix} &= \\textsf{D} \\begin{bmatrix}1 & \\nu & 0 \\\\ \\nu & 1 & 0 \\\\ 0 & 0 & (1-\\nu)/2 \\end{bmatrix}\\begin{Bmatrix}\\chi_{xx} \\\\ \\chi_{yy} \\\\ 2\\chi_{xy}  \\end{Bmatrix}\\\\\n",
    "\\text{ where } \\textsf{D} &= \\dfrac{E h^3}{12(1-\\nu^2)}\n",
    "\\end{align*}\n",
    "\n",
    "* Shear strain/stress relation:\n",
    "\\begin{align*}\n",
    "\\bQ &= \\textsf{F}\\bgamma\\\\\n",
    "\\text{ where } \\textsf{F} &= \\dfrac{5}{6}\\dfrac{E h}{2(1+\\nu)}\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec266ccd-8bad-4b7c-9f8a-740d55aa71b6",
   "metadata": {},
   "source": [
    "## Implementation\n",
    "\n",
    "We first load relevant modules and functions and generate a unit square mesh of triangles. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "68b597ca-fc49-4d14-8cee-2160a9996bc7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import ufl\n",
    "\n",
    "from mpi4py import MPI\n",
    "from dolfinx import fem, io\n",
    "from dolfinx.mesh import create_unit_square, CellType, locate_entities_boundary\n",
    "from ufl import (\n",
    "    as_matrix,\n",
    "    as_vector,\n",
    "    inner,\n",
    "    dot,\n",
    "    grad,\n",
    "    split,\n",
    "    FiniteElement,\n",
    "    VectorElement,\n",
    "    MixedElement,\n",
    "    TestFunction,\n",
    "    TrialFunction,\n",
    ")\n",
    "\n",
    "\n",
    "N = 10\n",
    "mesh = create_unit_square(MPI.COMM_WORLD, N, N, CellType.triangle)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b233274e-c9a5-4aa4-bb67-efc85c0b9bc2",
   "metadata": {},
   "source": [
    "Next we define material properties and functions which will be used for defining the variational formulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "b6011197-330f-4a77-b4ab-1fb48d11bb65",
   "metadata": {},
   "outputs": [],
   "source": [
    "# material parameters\n",
    "thick = 0.05\n",
    "E = 210.0e3\n",
    "nu = 0.3\n",
    "\n",
    "# bending stiffness\n",
    "D = fem.Constant(mesh, E * thick**3 / (1 - nu**2) / 12.0)\n",
    "# shear stiffness\n",
    "F = fem.Constant(mesh, E / 2 / (1 + nu) * thick * 5.0 / 6.0)\n",
    "\n",
    "# uniform transversal load\n",
    "f = fem.Constant(mesh, -100.0)\n",
    "\n",
    "# Useful function for defining strains and stresses\n",
    "def curvature(u):\n",
    "    (w, theta) = split(u)\n",
    "    return as_vector([theta[0].dx(0), theta[1].dx(1), theta[0].dx(1) + theta[1].dx(0)])\n",
    "\n",
    "def shear_strain(u):\n",
    "    (w, theta) = split(u)\n",
    "    return theta - grad(w)\n",
    "\n",
    "def bending_moment(u):\n",
    "    DD = as_matrix([[D, nu * D, 0], [nu * D, D, 0], [0, 0, D * (1 - nu) / 2.0]])\n",
    "    return dot(DD, curvature(u))\n",
    "\n",
    "def shear_force(u):\n",
    "    return F * shear_strain(u)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbe8f552-36d0-4f2f-b7a2-309255e63a67",
   "metadata": {},
   "source": [
    "Now we define the corresponding function space. Our dofs are $w$ and $\\btheta$, so the full function space $V$ will be a **mixed** function space consisting of a scalar subspace related to $w$ and a vectorial subspace related to $\\btheta$. We first use a continuous $P^2$ interpolation for $w$ and a continuous $P^1$ interpolation for $\\btheta$. We then define the corresponding linear and bilinear forms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2d0edfb4-9c4b-4300-ac80-353c84de63cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of function space for U:displacement, T:rotation\n",
    "Ue = FiniteElement(\"CG\", mesh.ufl_cell(), 2)\n",
    "Te = VectorElement(\"CG\", mesh.ufl_cell(), 1)\n",
    "V = fem.FunctionSpace(mesh, MixedElement([Ue, Te]))\n",
    "\n",
    "# Functions\n",
    "u = fem.Function(V, name=\"Unknown\")\n",
    "u_ = TestFunction(V)\n",
    "(w_, theta_) = split(u_)\n",
    "du = TrialFunction(V)\n",
    "\n",
    "# Linear and bilinear forms\n",
    "dx = ufl.Measure(\"dx\", domain=mesh)\n",
    "L = f * w_ * dx\n",
    "a = (dot(bending_moment(u_), curvature(du)) + dot(shear_force(u_), shear_strain(du)))*dx"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0cf1d500-2902-4e96-bff2-547248fe6846",
   "metadata": {},
   "source": [
    "Boundary conditions are now defined. We consider a fully clamped boundary. Note that since we are using a mixed function space, we cannnot use the `locate_dofs_geometrical` function. Instead, we locate the facets on the boundary using `dolfinx.mesh.locate_entities_boundary`. Then we locate the dofs on such facets using `locate_dofs_topological`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "e11fba71-6a33-4fb9-9825-cc53596756cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Boundary of the plate\n",
    "def border(x):\n",
    "    return np.logical_or(\n",
    "        np.logical_or(np.isclose(x[0], 0), np.isclose(x[0], 1)),\n",
    "        np.logical_or(np.isclose(x[1], 0), np.isclose(x[1], 1)),\n",
    "    )\n",
    "\n",
    "facet_dim = 1\n",
    "clamped_facets = locate_entities_boundary(mesh, facet_dim, border)\n",
    "clamped_dofs = fem.locate_dofs_topological(V, facet_dim, clamped_facets)\n",
    "\n",
    "u0 = fem.Function(V)\n",
    "bcs = [fem.dirichletbc(u0, clamped_dofs)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0304aef-7670-4803-814c-b78e692e7e71",
   "metadata": {},
   "source": [
    "We now solve the problem and output the result. To get the deflection $w$, we use `u.sub(0).collapse()` to extract a new function living in the corresponding subspace. Note that $u.sub(0)$ provides only an indexed view of the $w$ component of `u`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "8f7b8bf7-c2fa-4695-b381-de0d7634d92f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reissner-Mindlin deflection: 0.05180\n",
      "Love-Kirchhoff deflection: 0.05264\n"
     ]
    }
   ],
   "source": [
    "problem = fem.petsc.LinearProblem(\n",
    "    a, L, u=u, bcs=bcs, petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"}\n",
    ")\n",
    "problem.solve()\n",
    "\n",
    "with io.XDMFFile(mesh.comm, \"plates.xdmf\", \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "    w = u.sub(0).collapse()\n",
    "    w.name = \"Deflection\"\n",
    "    xdmf.write_function(w)\n",
    "\n",
    "w_LK = 1.265319087e-3 * -f.value / D.value\n",
    "print(f\"Reissner-Mindlin deflection: {max(abs(w.vector.array)):.5f}\")\n",
    "print(f\"Love-Kirchhoff deflection: {w_LK:.5f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f29f190-ebe7-45ba-8954-4fbdf42f0566",
   "metadata": {},
   "source": [
    "## Modal analysis\n",
    "\n",
    "Now we define the form corresponding to the definition of the mass matrix and we assemble the stiffness and mass forms into corresponding PETSc matrix objects. We use a value of 1 on the diagonal for K and 0 for M for the rows corresponding to the boundary conditions. Doing so, eigenvalues associated to boundary conditions are equal to infinity and will not pollute the low-frequency spectrum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "3a6c3cbf-2d20-44b5-a3af-0501b213d84b",
   "metadata": {},
   "outputs": [],
   "source": [
    "rho = fem.Constant(mesh, 1.0)\n",
    "m_form = rho * dot(du, u_) * dx\n",
    "\n",
    "K = fem.petsc.assemble_matrix(fem.form(a), bcs, diagonal=1)\n",
    "K.assemble()\n",
    "M = fem.petsc.assemble_matrix(fem.form(m_form), bcs, diagonal=0)\n",
    "M.assemble()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "633d5932-bf27-4973-b7b8-d8f7540aca32",
   "metadata": {},
   "source": [
    "We now use `slepc4py` to define a eigenvalue solver (EPS -- *Eigenvalue Problem Solver* -- in SLEPc vocable) and solve the corresponding generalized eigenvalue problem. Functions defined in the `eigenvalue_problem.py` module enable to define the corresponding objects, set up the parameters, monitor the resolution and extract the corresponding eigenpairs. Here the problem is of type `GHEP` (Generalized Hermitian eigenproblem) and we use a shift-invert transform to compute the smallest eigenvalues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "00c9f474-024a-4723-a9cf-76f4a3c188dd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "******************************\n",
      "***  SLEPc Iterations...   ***\n",
      "******************************\n",
      "Iter. | Conv. | Max. error\n",
      "    1 |     4 | 7.8e-07\n",
      "    2 |     8 | 1.9e-04\n",
      "\n",
      "******************************\n",
      "*** SLEPc Solution Results ***\n",
      "******************************\n",
      "Iteration number: 2\n",
      "Converged eigenpairs: 8\n",
      "\n",
      "Converged eigval.  Error \n",
      "----------------- -------\n",
      " 1.28e+02             1.2e-14\n",
      " 2.33e+02             4.7e-15\n",
      " 2.42e+02             1.5e-14\n",
      " 3.46e+02             2.9e-11\n",
      " 4.20e+02             1.6e-15\n",
      " 4.61e+02             8.0e-15\n",
      " 5.22e+02             2.6e-12\n",
      " 5.58e+02             2.5e-11\n"
     ]
    }
   ],
   "source": [
    "from slepc4py import SLEPc\n",
    "from eigenvalue_solver import solve_GEP_shiftinvert, EPS_get_spectrum\n",
    "\n",
    "N_eig = 6  # number of requested eigenvalues\n",
    "eigensolver = solve_GEP_shiftinvert(\n",
    "    K,\n",
    "    M,\n",
    "    problem_type=SLEPc.EPS.ProblemType.GHEP,\n",
    "    solver=SLEPc.EPS.Type.KRYLOVSCHUR,\n",
    "    nev=N_eig,\n",
    ")\n",
    "# Extract results\n",
    "(eigval, eigvec_r, eigvec_i) = EPS_get_spectrum(eigensolver, V)\n",
    "# Output eigenmodes\n",
    "with io.XDMFFile(mesh.comm, \"plates_eigenvalues.xdmf\", \"w\") as xdmf:\n",
    "    xdmf.write_mesh(mesh)\n",
    "    for i in range(N_eig):\n",
    "        w = eigvec_r[i].sub(0).collapse()\n",
    "        w.name = \"Deflection\"\n",
    "        xdmf.write_function(w, i)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "298ac5f1-f5ef-4d55-9ce4-9a5cf010e852",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
